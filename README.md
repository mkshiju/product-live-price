UI Setup
**********
Polymer 2.0
This application is for showing list of instruments with their live price updated values.
The Technology used is Polymer 2.0
For seeing the application,
## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. 
Then run `polymer serve` to serve your application locally.

## If bower_componets folder not there in the application folder, run
```
$ bower install
```
## Viewing  Application
```
$ polymer serve
```
## Building  Application
```
$ polymer build
```
This will create builds of application in the `build/` directory, optimized to be served in production.

-------------------------------------------------------------------------------------------------------------------

JAVA setup
***********

# Securities-Case
Build this project by;

    $ mvn clean install
	$ cd securities-case-web
	$ mvn spring-boot:run

## Main subprojects

### securities-case-service

`securities-case-services` implements the services for this application and contains its domain model.
The domain model is simple. There is an Instrument class and a Price class. An instrument has a code and price.
A price has a timestamp and an amount. The services module contains the `InstrumentService` (spring component) which can be used to retrieve all instruments with their latest prices. It is also possible to register a InstrumentListener which will callback in case a price of an instrument is updated.
To simulate real instrument price updates a spring scheduler is used which randomly updates prices every second and triggers the callback.


### securities-case-web

securities-case-web implements a REST API and contains a websocket for price updates.
After building this project it may be started by: 

    $ mvn spring-boot:run
    
Or by running the main class:

    securities.web.BootApp
    
Open the URL: http://localhost:8080/instruments to retrieve the instruments.
Open the URL: http://localhost:8080/ to see an example of a page which receives price updates over websockets
    
The `webapp` folder contains a minimal client implementation to connect with the web socket as an example.

## The Assignment

Use the REST API and the web socket connection to show the instruments and their prices on a web page.
The prices should automatically update and highlight on update.

