This application is for showing list of instruments with their live price updated values.
The Technology used is Polymer 2.0

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## If bower_componets folder not there in the application folder, run

```
$ bower install
```

## Viewing  Application

```
$ polymer serve
```

## Building  Application

```
$ polymer build
```

This will create builds of application in the `build/` directory, optimized to be served in production.


