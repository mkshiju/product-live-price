This application is for showing list of instruments with their live price updated values.

The application has been built in 3 technologies 1)Polymer 2.0, 2) angular 4.0, 3) reactjs

In Polymer, I have implemented optimized code, also has been integrated test cases.
In angular and reactjs implemented the functionalities, still can optimize and need to integrate test cases .

How to do setup, build and view in polymer
-----------------------------------------

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your application locally.

## If bower_componets folder not there in the application folder, run

```
$ bower install
```

## Viewing  Application

```
$ polymer serve
```

## Building  Application

```
$ polymer build
```

This will create builds of application in the `build/` directory, optimized to be served in production.


