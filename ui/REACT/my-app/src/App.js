import React, { Component } from 'react';
import './App.css';
import $ from "jquery";
import SockJsClient from "react-stomp";
import 'whatwg-fetch';

class App extends Component {
      
  constructor(props) {
    super(props);
    this.state = {compare: false};
  }

  componentDidMount() {    
    var that = this;
      var url = 'http://localhost:8080/instruments';
    fetch(url)
    .then(function(response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    })
    .then(function(data) {
      that.setState({ productList: data });
      console.log(that.state)
    });
  }
  
  render() {
    return (
      <div className="App">
      <table id="products">
      <tr>
        <th>Code</th>
        <th>Date</th>
        <th>Price</th>
      </tr>
        {this.state.productList && this.state.productList.map(val => 
      <tr>
        <td>{val.code}</td>
        <td>{new Date(val.price.when).toString()}</td>
        <td>{val.price.amount}</td>
      </tr>
        )}
        </table>

        <SockJsClient url='http://localhost:8080/stomp' topics={['/topic/update']}
            onMessage={(msg) => { 
              console.log(msg); 
              var indexVal;
              var newArr = this.state.productList.map(function(elem,index){
                if(elem.code == msg.code){
                  indexVal = index;
                  elem.price.when = msg.price.when;
                  elem.price.amount = msg.price.amount;
                }
                return elem;})
                this.setState({productList: newArr})  
                console.log(indexVal);   
                var x = document.getElementsByTagName("tr");
                x[indexVal+1].style.backgroundColor = 'yellow';
                window.setTimeout(function(){
                  x[indexVal+1].style.backgroundColor = 'white';
                },300)
              }}
            ref={(client) => { this.clientRef = client }} />
      </div>
    );
  }
}

export default App;
