import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { HttpClient } from '@angular/common/http';
import {Message} from '@stomp/stompjs';
import {StompService} from '@stomp/ng2-stompjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  constructor(private http: HttpClient, private stompService: StompService) { }

  title = 'app';
  productList = null;
  indexVal = null;
  url = 'http://localhost:8080/instruments';

  private subscription: Subscription;
  public messages: Observable<Message>;

  ngOnInit() {
    this.http.get(this.url).subscribe(data => {
      this.productList = data;
      this.productList.forEach(elem => {
        elem.price.when = new Date(elem.price.when);
      });
      console.log(this.productList);
    });
    this.messages = this.stompService.subscribe('/topic/update');

    // Subscribe a function to be run on_next message
    this.subscription = this.messages.subscribe(this.on_next);
    console.log(this.productList);
  }

  public on_next = (message: Message) => {

    var msg = JSON.parse(message.body);
    this.productList = this.productList.map(function(elem,index){
      if(elem.code == msg["code"]){
        elem.price.when = new Date(msg["price"]["when"]);
        elem.price.amount = msg["price"]["amount"]
      }
      return elem;})
      this.indexVal = this.productList.findIndex(item => item.code == msg.code);
      console.log(this.indexVal);
    // Log it to the console
    console.log(message.body);
  }
}
